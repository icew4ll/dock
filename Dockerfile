FROM ubuntu:latest
MAINTAINER icew4ll
 
ENV HOME=/root
ENV bld=$HOME/build
ENV GOPATH=$bld/.go
ENV GOBIN=$GOPATH/bin
ENV GOROOT=$bld/go
ENV PATH="/usr/lib/freecad-daily/lib:$HOME/.cargo/bin:/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games:$HOME/.local/bin:$HOME/n/bin:$GOPATH/bin:$GOROOT/bin:$N_PREFIX/bin:$HOME/.yarn/bin:/snap/bin:$HOME/.pyenv/bin:/opt:$HOME/v:$HOME/.emacs.d/bin:$HOME/bin:$HOME/build/.go/bin:$HOME/.volta/bin"

RUN apt-get -y update
RUN apt-get -y upgrade
RUN apt-get install -y build-essential curl git zsh bash openssl openssh-client clang ninja-build python3-pip python2 unzip libssl-dev libxcb-shape0-dev libx11-xcb-dev m4 libx11-dev libxrandr-dev language-pack-en tmux xclip nmap proxychains4 rdesktop ranger
# error
# RUN apt-get install -y pkg-config

# miniminal
# RUN apt-get install -y build-essential git zsh curl libssl-dev

ARG dir=/root/ide
ARG scripts=$dir/scripts
COPY . $dir
WORKDIR $dir
RUN $scripts/install.sh
