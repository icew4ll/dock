#!/bin/bash

dock=$(dirname "$(dirname "$(readlink -f "$0")")")
file=$dock/Dockerfile
mkdir -p "$(dirname "$file")"
cat <<'EOF' >$file
FROM ubuntu:latest
MAINTAINER icew4ll
 
ENV HOME=/root
ENV bld=$HOME/build
ENV GOPATH=$bld/.go
ENV GOBIN=$GOPATH/bin
ENV GOROOT=$bld/go
ENV PATH="/usr/lib/freecad-daily/lib:$HOME/.cargo/bin:/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games:$HOME/.local/bin:$HOME/n/bin:$GOPATH/bin:$GOROOT/bin:$N_PREFIX/bin:$HOME/.yarn/bin:/snap/bin:$HOME/.pyenv/bin:/opt:$HOME/v:$HOME/.emacs.d/bin:$HOME/bin:$HOME/build/.go/bin:$HOME/.volta/bin"

RUN apt-get -y update
RUN apt-get -y upgrade
RUN apt-get install -y build-essential curl git zsh bash openssl openssh-client clang ninja-build python3-pip python2 unzip libssl-dev libxcb-shape0-dev libx11-xcb-dev m4 libx11-dev libxrandr-dev language-pack-en tmux xclip nmap proxychains4 rdesktop ranger
# error
# RUN apt-get install -y pkg-config

# miniminal
# RUN apt-get install -y build-essential git zsh curl libssl-dev

ARG dir=/root/ide
ARG scripts=$dir/scripts
COPY . $dir
WORKDIR $dir
RUN $scripts/install.sh
EOF
cat "$file"

# docker build
docker stop $(docker ps -aq)
docker rm -f "$(docker ps -aq)"
docker rmi -f "$(docker images -a | grep -v ubuntu | awk '{print $3}')"
name=ub_ide
img=$DOCKER_ID_USER/$name:latest
dock=~/m/dock
(cd "$dock" && bash ./scripts/build.sh && docker build --tag "$img" .)

# run docker and enable clipboard sharing with host
name=ub_ide
img=$DOCKER_ID_USER/$name
xhost local:root
docker run -d -e DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix "$img" tail -f /dev/null
con=$(docker ps -q)
docker exec -it "$con" /bin/zsh
