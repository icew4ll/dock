bandwhich v0.18.1:
    bandwhich
bat v0.15.4:
    bat
bb v0.4.0:
    bb
bottom v0.4.7:
    btm
broot v1.0.0:
    broot
caretaker v0.1.4:
    caretaker
cargo-edit v0.6.0:
    cargo-add
    cargo-rm
    cargo-upgrade
cargo-outdated v0.9.11:
    cargo-outdated
cargo-update v4.1.1:
    cargo-install-update
    cargo-install-update-config
cargo-watch v7.5.0:
    cargo-watch
choose v1.3.1:
    choose
code-minimap v0.3.0:
    code-minimap
diesel_cli v1.4.1:
    diesel
dijo v0.2.3:
    dijo
dua-cli v2.10.2:
    dua
eva v0.2.7:
    eva
exa v0.9.0:
    exa
fd-find v8.1.1:
    fd
fontfor v0.3.1:
    fontfor
gifski v1.2.1:
    gifski
gitoxide v0.1.0:
    gio
    giop
gitoxide v0.4.0:
    gix
    gixp
gitui v0.10.1:
    gitui
grex v1.1.0:
    grex
hors v0.6.10:
    hors
hunter v1.3.5:
    hunter
    hunter-media
hyperfine v1.10.0:
    hyperfine
joshuto v0.8.2 (/home/ice/m/joshuto):
    joshuto
kalk_cli v0.1.8:
    kalk_cli
kalk_cli v0.2.0:
    kalk
ktrl v0.1.8:
    ktrl
lolcate-rs v0.8.0:
    lolcate
lsd v0.18.0:
    lsd
mdcat v0.21.1:
    mdcat
navi v2.9.0:
    navi
nu v0.19.0:
    nu
    nu_plugin_core_fetch
    nu_plugin_core_inc
    nu_plugin_core_match
    nu_plugin_core_post
    nu_plugin_core_ps
    nu_plugin_core_sys
    nu_plugin_core_textview
pastel v0.8.0:
    pastel
ripgrep v12.1.1:
    rg
rustscan v1.8.0:
    rustscan
sad v0.1.5 (https://github.com/ms-jpq/sad#fccecf8f):
    sad
sd v0.7.6:
    sd
sfz v0.2.1:
    sfz
shellharden v4.1.2:
    shellharden
shotgun v2.2.0:
    shotgun
simple-http-server v0.6.1:
    simple-http-server
slap-cli v1.2.0:
    slap
starship v0.44.0:
    starship
suckit v0.1.0 (https://github.com/skallwar/suckit#c5bed43e):
    suckit
tab v0.3.7:
    tab
titlecase v1.1.0:
    titlecase
ttyplot-rs v0.1.0 (https://github.com/mogenson/ttyplot-rs#e790cb8e):
    ttyplot-rs
tunnelto v0.1.12:
    tunnelto
viu v1.0.0:
    viu
voidmap v1.1.5:
    void
watchexec v1.14.0:
    watchexec
wormhole-tunnel v0.1.5:
    wormhole
    wormhole_server
xcolor v0.4.0:
    xcolor
ytop v0.6.2:
    ytop
zoxide v0.4.3:
    zoxide
