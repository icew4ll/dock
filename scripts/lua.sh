#!/bin/sh
set -x
build="$1"
# make install lua
cd $(find "$build" -maxdepth 1 -type d -regextype posix-egrep -regex ".*lua-[0-9].*")
make linux test
make install

# make lua ls
cd $(find "$build" -maxdepth 1 -type d -regextype posix-egrep -regex ".*lua-language.*")/3rd/luamake
ninja -f ninja/linux.ninja
cd ../..
./3rd/luamake/luamake rebuild
