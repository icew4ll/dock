#!/bin/sh
nvim --headless "+version" +qa
nvim --headless "+UpdateRemotePlugins" +qa
# nvim --headless "+LspInstall sumneko_lua"
nvim --headless "+LspInstall bashls" +qa
nvim --headless "+LspInstall vimls" +qa
nvim --headless "+LspInstall tsserver" +qa
nvim --headless "+LspInstall yamlls" +qa
nvim --headless "+LspInstall vuels" +qa
nvim --headless "+LspInstall html" +qa
nvim --headless "+LspInstall jsonls" +qa
# nvim --headless "+TSInstall lua python go yaml json markdown html typescript vue regex toml rust bash css javascript tsx"
# nvim --headless "+TSInstallInfo" +qa
# nvim --headless "+Clap install-binary" +qa

# works
# nvim --headless "+TSInstall lua python go yaml json markdown html typescript vue regex toml rust bash css javascript tsx" "+Clap install-binary"
# nvim --headless "+TSInstallInfo" +qa
# watch -n 0.1 du -s ~/.config/nvim/repos/github.com
# ~/.local/share/nvim
# ~/.config/nvim/.cache/init.vim
