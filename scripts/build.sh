#!/bin/bash

set -x
# check prereqs
if ! [ -x "$(command -v git)" ]; then
	echo 'Error: git is not installed.' >&2
	exit 1
fi

if ! [ -x "$(command -v curl)" ]; then
	echo 'Error: curl is not installed.' >&2
	exit 1
fi

if ! [ -x "$(command -v gcc)" ]; then
	echo 'Error: gcc is not installed.' >&2
	exit 1
fi

if ! [ -x "$(command -v docker)" ]; then
	echo 'Error: docker is not installed.' >&2
	exit 1
fi

if ! [ -x "$(command -v gunzip)" ]; then
	echo 'Error: gunzip is not installed.' >&2
	exit 1
fi

# setup build dir
build=$(dirname "$(dirname "$(readlink -f "$0")")")/build
scripts=$(dirname "$(dirname "$(readlink -f "$0")")")/scripts
[ -d "$build" ] && rm -rf "$build"
mkdir -p "$build"

# download helper functions
downer() {
	file=$build/$1
	url=$2
	echo "Downloading $file from $url"
	curl -fsSL "$url" >"$file" && chmod +x "$file"
}

giter() {
	url=$1
	gitdir=$build/$(echo "${url##*/}")
	[ -d "$gitdir" ] && echo "$(cd "$gitdir" && git pull)" || (cd "$build" && git clone "$url")
}

system() {
	# nvim download
	"$scripts"/nvim.sh "$build"
	# oh-my-tmux
	url=https://github.com/gpakosz/.tmux
	giter "$url"
	# zinit
	file=zinit.sh
	url=https://raw.githubusercontent.com/zdharma/zinit/master/doc/install.sh
	downer "$file" "$url"
	# get dot files
	url=https://gitlab.com/icew4ll/public
	giter "$url"
}

python_install() {
	# pip
	file=get-pip.py
	url=https://bootstrap.pypa.io/get-pip.py
	downer "$file" "$url"
}

node_install() {
	# node download
	# file=n-install.sh
	# url=https://git.io/n-install
	# downer "$file" "$url"
	file=volta.sh
	url=https://get.volta.sh
	downer "$file" "$url"
}

lua() {
	# sumneko lua ls
	url=https://github.com/sumneko/lua-language-server
	giter "$url"
	cd "$(find "$build" -maxdepth 1 -type d -regextype posix-egrep -regex ".*lua-language.*")" && git submodule update --init --recursive

	# download latest lua
	url=http://www.lua.org/ftp
	lat=$(curl -L "$url" | egrep ".*lua-[0-9].*" | sed "s/^.*HREF=\"\(.*\)\".*/\1/" | head -1)
	dl=$url/$lat
	cd "$build" && curl -sL "$dl" | tar xz

}

go_install() {
	url=https://storage.googleapis.com/golang
	file=$(curl -s "$url" | sed 's/.*\(go.*amd64.tar.gz\).*/\1/p' | head -1)
	(cd "$build" && curl -O "$url/$file" && tar xzf "$file" && rm go*tar.gz)
}

rust() {
	# rust
	file=rustup.sh
	url=https://sh.rustup.rs
	downer "$file" "$url"

	repo="rust-analyzer/rust-analyzer"
	url="https://api.github.com/repos/$repo/releases"
	tag=linux.gz
	dl=$(curl -sL "$url" | grep "browser_download_url" | awk '{print $2}' | sed 's/\"//g' | grep "nightly" | grep "$tag")
	file=$(echo "${dl##*/}")
	cd "$build" && curl -Lo "$file" "$dl" && gunzip "$file" && chmod +x "${file%.gz}"
}

system
python_install
node_install
go_install
lua
rust
