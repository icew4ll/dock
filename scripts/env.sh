#!/bin/sh
# find latest py in /usr/bin
dir=/usr/bin
py3=$(find $dir -type f -regextype posix-egrep -regex ".*python3.[0-9]$"|tail -1)
py2=$(find $dir -type f -regextype posix-egrep -regex ".*python2.[0-9]$"|tail -1)
echo $py2 $py3
# replace python versions
file=$1/public/bak/.config/nvim/python.vim
fd="let g:python_host_prog = '.*'"
sed -i "s#$fd#let g:python_host_prog = '$py2'#" $file
fd="let g:python3_host_prog = '.*'"
sed -i "s#$fd#let g:python3_host_prog = '$py3'#" $file
