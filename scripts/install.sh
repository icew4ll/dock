#!/bin/bash

# declare directories based on location of script
set -x
base=$(dirname "$(dirname "$(readlink -f "$0")")")
build=$base/build
scripts=$base/scripts

system() {
	# install scripts
	ln -s "$build"/nvim-linux64/bin/nvim /usr/local/bin
	"$build"/zinit.sh -y
	"$scripts"/tmux.sh "$build"
	"$scripts"/lua.sh "$build"
	# load configs before applying fix and source
	"$build"/public/load.sh "$build"/public/bak
	"$scripts"/fix.sh "$build"
	zsh -c "source ~/.zshrc"
	chsh -s /usr/bin/zsh && echo "$SHELL"
	# chmod 660 ~/.zsh_history
}

python_install() {
	python2 "$build"/get-pip.py
	pip2 install --user pynvim
	pip3 install --user pynvim
	python3.8 -m pip install --upgrade pip
	python3.8 -m pip install 'python-language-server[all]'
	# python3.8 -m pip install -e git://github.com/davidhalter/jedi.git#egg=jedi
	python3.8 -m pip install pyls-mypy pyls-isort pyls-black youtube-dl black toml-sort httpie
}

node_install() {
	"$build"/volta.sh
	volta install node@latest yarn rome neovim lua-fmt prettier javascript-typescript-langserver npm-check-updates typescript gridsome netlify-cli vercel
}

go_install() {
	# create build/bin dirs
	dest=~/build && mkdir -p "$dest"
	bin=~/bin && mkdir -p "$bin"
	# move go from $build to ~/build
	[[ -d $dest/go ]] && rm -rf "$dest"/go
	mv "$build"/go "$dest"
	# link go to bin
	[[ -f $bin/go ]] && rm "$bin"/go
	ln -s "$dest"/go/bin/go "$bin"
	# make go path
	mkdir -p "$dest"/.go/{bin,src,pkg}
	# install gobin
	GO111MODULE=off
	go get -u github.com/myitcv/gobin
	# install shfmt
	gobin github.com/mvdan/sh/cmd/shfmt
	gobin github.com/chriswalz/bit
	gobin github.com/chriswalz/bit/bitcomplete
}

rust_install() {
	# install rust
	"$build"/rustup.sh -y
	rustup toolchain add nightly
	rustup component add rust-src rustfmt clippy rust-analysis
	mv "$build"/rust-analyzer-linux /usr/local/bin/rust-analyzer
	cargo +nightly install racer
	cargo install xcolor bat fd-find ripgrep kalk_cli lsd watchexec viu shellharden grex cargo-single git-delta
	# cargo install cargo-update cargo-outdated cargo-edit mdcat shotgun
	# verify autocompletions
	# racer complete std::io::
}

lsp() {
	# install lsp
	zsh "$scripts"/lsp.sh
}

python_install
node_install
go_install
rust_install
system
lsp
