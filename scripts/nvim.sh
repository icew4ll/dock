#!/bin/sh
repo="neovim/neovim"
url="https://api.github.com/repos/$repo/releases"
dl=$(curl -s $url |
	grep "browser_download_url" |
	grep "nightly" |
	awk '{print $2}' |
	sed 's/\"//g' |
	grep "linux64")
file=$(echo $dl | sed "s/^.*\/\(.*\).tar.gz/\1/")
cd "$1" && curl -sL $dl | tar xz
