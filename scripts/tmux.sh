#!/bin/sh
cp -r $1/.tmux $HOME
cd $HOME
ln -s -f .tmux/.tmux.conf
cp .tmux/.tmux.conf.local .
